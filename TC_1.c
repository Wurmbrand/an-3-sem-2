//Lambda-NFA

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void read_from_file()
{
        //Open the input file
        FILE *fp;
	fp = fopen("~NFA.txt", "r");
        char reader[255];
	char alphabet[50];
	char ch;
	int iterator, no_states, start_state, no_final, no_alphabet;
	int final_states[50];
        /*
         * From MY writing convention the first row
         * is an integer number (no_states) of the
         * states of our ~NFA and the states are called
         * from 0 to (no_states - 1)
         */
        fscanf(fp, "%s", reader);
        /*
         * The conversion into an integer value
         * because we read form file a string
         */
        no_states = atoi(reader);
        
	/*
	 * On the second row we assume that the start state is
	 */
	fscanf(fp, "%s", reader);
	start_state = atoi(reader);
	/*
	 * On the third row is written firstly the number
	 * of the final states, then ":" and the final states ID
	 */
	fscanf(fp, "%s", reader);
	no_final = atoi(reader);
	fscanf(fp, "%s", reader);//here we pass :

	//Here I take the final states
	for(iterator=0; iterator<no_final; iterator++){
		fscanf(fp, "%s", reader);
		final_states[iterator] = atoi(reader);
	}

	fscanf(fp, "%s", reader);
	no_alphabet = atoi(reader);
	fscanf(fp, "%s", reader);//here we pass :

	//Here I take the alphabet
	for(iterator=0; iterator<no_alphabet; iterator++){
		alphabet[iterator] = getc(fp);
		if (alphabet[iterator] == ' ')
			iterator--;
	
	}

	
	
	printf("The number of the states: %d\n", no_states);
	printf("The start state of the NFA is :%d\n", start_state);

	printf("The final states are: ");
	for(iterator=0; iterator<no_final; iterator++)
		printf("%d\t", final_states[iterator]);
	
	printf("The alphabet is: ");
	for(iterator=0; iterator<no_alphabet; iterator++)
                printf("%c\t", alphabet[iterator]);

}

//This function is a validation of the final state
void final_state_validation(int cur_state)
{


}
int main()
{
read_from_file();

return 0;
}
