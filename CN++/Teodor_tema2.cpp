#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

int n, p;
double a[100][100];
double at[100][100];
long double l[100][100];
double u[100][100];
double y[100];
double b[100];
double x[100];
long double q[100][100];
long double r[100][100];

double combinari(int n,int k){
  if (k==0)
    return 1;
  else
    if (k>n)
      return 0;
    else
      return (combinari(n-1,k)+combinari(n-1,k-1));
}//combinari

void calculeazaMatrice(int n, int p) {
  int i, j;
  double comb_n, comb_k;
  for(i=1;i<=n;i++)
    a[1][i] = 1;
  for(i=2;i<=n;i++)
    a[i][1] = a[i-1][1] *((double)(p-i+2)/(i-1));
  for (i =1; i <=n; i++)
    for (j=1; j<= n; j++){
            if(i!=1 && j!=1)
      a[i][j] = a[i-1][j-1] + a[i][j-1];
    }//for j


for (i = 1; i <= n; i++)
    b[i] = 1;
  // Afisare matrice
  cout << "Matricea A:" << endl;
  for (i =1; i <=n; i++) {
    for (j=1; j<= n; j++)
      cout << a[i][j] << " ";
    cout << endl;
  }//for i
  cout << endl ;
}//calculeazaMatrice


void calculeazaLU() {
  int k,i;
  double suma;

  // Primul pas
  for(int i=1; i<=n; i++)
    l[i][1] = a[i][1];

  u[1][1] = 1;

  for(int j=2; j<=n; j++) {
    if (l[1][1] != 0)
      u[1][j] = a[1][j] / l[1][1];
    else {
      cout << "Metoda nu se aplica ";
      return;
    }//else
  }//for j

  for(k = 2; k <= n; k++) {
    for(i = k; i <= n; i++) {
      // prima suma
      suma = 0;
      for(p=1; p<=k-1; p++)
        suma += l[i][p] * u[p][k];
      l[i][k] = a[i][k] - suma;
    }//for i
    u[k][k] = 1;
    // a doua suma
    for (int j= k+1; j<=n; j++) {
      suma = 0;
      for (int p=1; p<=k-1; p++)
        suma += l[k][p] * u[p][j];
      int numarator = a[k][j] - suma;
      if (l[k][k] != 0)
        u[k][j] = numarator / l[k][k];
      else {
        cout << "Metoda nu se aplica";
        return;
      }//else
    }//for j
  }//for k

  // Afisare L
  cout << "Matricea L:" << endl ;
  for (int i = 1; i <=n; i++) {
    for (int j=1; j <= n; j++)
      cout << l[i][j] << " ";
    cout << endl;
  }//for i
  cout << endl;

  // Afisare U
  cout << "Matricea U:"  << endl ;
  for (int i = 1; i <=n; i++) {
    for (int j=1; j <= n; j++)
      cout << u[i][j] << " ";
    cout << endl;
  }//for i
  cout << endl;
}//calculeazaLU

void calculeazaSistemLU() {
  double suma, numarator;
  int i, k;
  // Initializare vectori coloana
  for (int i = 1; i <= n; i++) {
    b[i] = 1;
    y[i] = 0;
  }//for i
  for(i = 1; i<=n; i++) {
    suma = 0;
    for(k = 1; k <= i-1; k++)
      suma += l[i][k] * y[k];
    numarator = b[i] - suma;
    if ( l[i][i] != 0)
      y[i] = numarator / l[i][i];
    else {
      cout << "Metoda nu se aplica!";
      return;
    }//else
  }//for i
}//calculeazaSistem

double sol[100];
void calculeazaXLU() {
  int i, j, k;
  double suma;
  x[1] = 0;

  for (i = n; i >= 1; i--) {
    suma = 0;
    for (int k = i+1; k<=n; k++)
      suma += u[i][k] * x[k];
    x[i] = y[i] - suma;
  }//for i

  // Afisare X
  cout << "Solutia este: ";
  for (int i =1; i <= n; i++) {
    sol[i] = x[i];
    cout << round(x[i]) << " ";
  }
  cout << endl;
}//calculeazaX


//Choleski

int matriceSimetricaCh() {
  int i, j;
  for (i = 1; i <= n; i++)
    for (j = i+1; j <= n; j++)
      if (a[i][j] != a[j][i])
        return 0;
  return 1;
}//matriceSimetrica

int matricePozitivDefinitaCh() {
  int j, k;
  double suma;
  for (j = 1; j <= n; j++) {
    suma = 0;
    for (k = 1; k < j; k++)
      suma += l[j][k] * l[j][k];
    if ((a[j][j] - suma) <= 0)
      return 0;
  }//for j
  return 1;
}//matricePozitivDefinita

void calculeazaTranspusaCh() {
  int i, j;

  for (i = 1; i <= n; i++)
    for (j = 1; j <= n; j++)
      at[i][j] = a[j][i];
}



double AtA[100][100];
double AtB[100];
void recalculeazaMatriceCh() {
  int i, j, k;

  calculeazaTranspusaCh();

  for (i = 1; i <= n; i++)
    for (j = 1; j <= n; j++) {
      AtA[i][j] = 0;
      for (k = 1; k <= n; k++)
        AtA[i][j] += at[i][k] * a[k][j];
    }//for j

  for (i = 1; i <= n; i++) {
    AtA[i][j] = 0;
    for (k = 1; k <= n; k++)
      AtB[i] += at[i][k] * b[k];
  }//for i

  //Inlocuieste A cu AtA si B cu AtB

  for (i = 1; i <= n; i++) {
    b[i] = AtB[i];
    for (j = 1; j <= n; j++)
      a[i][j] = AtA[i][j];
  }//for i

  cout << "Matricea recalculata AtA:" << endl;
  for (i =1; i <=n; i++) {
    for (j=1; j<= n; j++)
      cout << a[i][j] << " ";
    cout << endl;
  }//for i
  cout << endl ;

  cout << "Matricea recalculata AtB:" << endl;
  for (i =1; i <=n; i++)
    cout << b[i] << " ";
  cout << endl << endl ;
}//recalculeazaMatrice

void calculeazaLCh() {
  int i, j, k;
  long double suma, partial;


  l[1][1] = sqrt(a[1][1]);

  if (l[1][1] == 0){
    cout << "Metoda nu se aplica!";
    return;
  }//if
  for (i = 2; i <= n; i++) {
    l[i][1] = a[i][1]/l[1][1];
  }//for i

  for (j = 2; j <= n; j++) {
    suma = a[j][j];
    for (k = 1; k < j; k++)
      suma -= l[j][k]*l[j][k];
    //cout<<endl<<(suma)<<endl;
    l[j][j] = sqrt(suma);

    if (l[j][j] == 0) {
      cout << "Metoda nu se aplica!";
      return;
    }//if

    for (i = j+1; i <= n; i++) {
      suma = a[i][j]/l[j][j];
      for (k = 1; k < j; k++)
        suma -= l[i][k]/l[j][j] * l[j][k];
      l[i][j] = suma;
    }//for i
  }//for j

  // Afisare L
  cout << "Matricea L:" << endl ;
  for (i = 1; i <= n; i++) {
    for (j = 1; j <= n; j++)
      cout << l[i][j] << " ";
    cout << endl;
  }//for i
  cout << endl;
}//calculeazaQR


void rezolvaSistemCh() {
  double suma;
  int i, j, k;

  for (i = 1; i <= n; i++) {
    if (l[i][i] == 0){
      cout << "Metoda nu se aplica!";
      return;
    }//if

    suma = 0;
    for (k = 1; k < i; k++)
      suma += l[i][k]*y[k];
    y[i] = (b[i] - suma) / l[i][i];
  }//for i

  for (i = n; i >= 1; i--) {
    if (l[i][i] == 0) {
      cout << "Metoda nu se aplica!";
      return;
    }//if

    suma = 0;
    for (k = i+1; k <= n; k ++)
      suma += l[k][i]*x[k];
    x[i] = (y[i] - suma) / l[i][i];
  }//for i

  // Afisare X
  cout << "Solutia este: ";
  for (int i =1; i <= n; i++) {
    sol[i] = x[i];
    cout << round(x[i]) << " ";
  }
  cout << endl;
}//rezolvaSistem

double V[100];



//QR

void calculeazaQR() {
  int i, j, k;
  long double suma;
  suma = 0;
  for(i=1;i<=n;i++)
    for(j=1;j<=n;j++)
        q[i][j] = 0;

  for (i = 1; i <= n; i++)
    suma += a[i][1] * a[i][1];
  r[1][1] = sqrt(suma);
  if (r[1][1] == 0){
    cout << "Metoda nu se aplica!";
    return;
  }//if
  for (i = 1; i <= n; i++)
    q[i][1] = a[i][1] / r[1][1];
  for (k = 2; k <= n; k++) {
    suma = 0;
    for (j = 1; j < k; j++) {
      suma = 0;
      for (i = 1; i<=n; i++)
        suma += a[i][k]*q[i][j];
      r[j][k] = suma;
    }//for j

    suma = 0;
    for (i = 1; i <= n; i++)
      suma += a[i][k]*a[i][k];
    r[k][k] = suma;
    suma = 0;
    for (i = 1; i < k; i++)
      suma += r[i][k]*r[i][k];
    r[k][k] -= suma;
    r[k][k] = sqrt(r[k][k]);

    if (r[k][k] ==  0) {
      cout << "Metoda nu se aplica!";
      return;
    }//if
    for (i = 1; i <= n; i++) {
      suma = 0;
      for (j = 1; j < k; j++)
        suma += r[j][k] * q[i][j];
      q[i][k] = (1 / r[k][k]) * (a[i][k] - suma);
    }//for i
  }//for k

  // Afisare Q
  cout << "Matricea Q:" << endl ;
  for (i = 1; i <= n; i++) {
    for (j = 1; j <= n; j++)
      cout << q[i][j] << " ";
    cout << endl;
  }//for i
  cout << endl;

  // Afisare R
  cout << "Matricea R:"  << endl ;
  for (i = 1; i <=n; i++) {
    for (j = 1; j <= n; j++)
      cout << r[i][j] << " ";
    cout << endl;
  }//for i
  cout << endl;

}//calculeazaQR


void rezolvaSistemQR() {
  double suma;
  int i, j;
  for (i = 1; i <= n; i++)
    b[i] = 1;
  for (i=1; i <= n; i++) {
    suma = 0;
    for (j=1; j <= n; j++) {
      suma += q[j][i] * b[j]; //nu ca in carte
    }//for j
    y[i] = suma;
  }//for i

  if (r[n][n] == 0) {
    cout << "Metoda nu se aplica!";
    return;
  }//if
  x[n]=y[n] / r[n][n];

  for (i=n-1; i>=1; i--) {
    if (r[i][i] == 0) {
      cout << "Metoda nu se aplica!";
      return;
    }//if
    suma = 0;
    for (j=i+1; j <= n; j++)
      suma += r[i][j] * x[j];
    x[i] = (1 / r[i][i]) * (y[i] - suma);
  }//for i

  // Afisare X
  cout << "Solutia: ";
  for (int i =1; i <= n; i++) {
    sol[i] = x[i];
    cout << round(x[i]) << " ";
  }
  cout << endl;
}//rezolvaSistem





int main() {
  int i, j, k;
  int n1 = 5, p1 = 10, n2 = 10, p2 = 20, n3 = 20, p3 = 50;
  n = n2;
  p = p2;
    int val;
  cout<<"1. LU\n2. Cholesky\n3. QR\n\n";
  while(true)
  {
      cin>>val;
      if(val==1)
      {

          cout<<endl;
          cout<<"n=";
          cin>>n;
          cout<<"p=";
          cin>>p;
          calculeazaMatrice(n,p);
          calculeazaLU();
          calculeazaSistemLU();
          calculeazaXLU();
          cout<<endl<<endl;
      }
      if(val == 2)
      {
          cout<<endl;
          cout<<"n=";
          cin>>n;
          cout<<"p=";
          cin>>p;
          calculeazaMatrice(n,p);
          if (matriceSimetricaCh())
              calculeazaLCh();
          if ((!matriceSimetricaCh()) || (!matricePozitivDefinitaCh())) {
                recalculeazaMatriceCh();
            calculeazaLCh();
            }//if
            rezolvaSistemCh();
      }
      if(val == 3)
      {
          cout<<endl;
          cout<<"n=";
          cin>>n;
          cout<<"p=";
          cin>>p;
          calculeazaMatrice(n,p);
          calculeazaQR();
          rezolvaSistemQR();
      }
      cout<<"\n1. LU\n2. Cholesky\n3. QR\n\n";
  }



  return 0;
}//main
