#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <cstdlib>

using namespace std;

double sigma = pow(10, -10);
double epsilon = pow(10,-10);
double low = 0.0;
double high = 3.0;
int maxiters = 500;
int intervals = 12;

double newton(double, double);
double coardei(double, double);
double bisectiei(double, double);
double falsei(double, double);
double secantei(double, double);

double(*mets[])(double, double) = { &bisectiei, &falsei, &coardei, &secantei, &newton };
const double e = 2.71828182845;

double f(double x) {
	return 4 * exp(-x)*sin(6 * x*sqrt(x)) - (1 / 5.0);
}

double f1(double x) {
	return (-4)*exp(-x)*sin(6 * x*sqrt(x)) + 4 * exp(-x)*cos(6 * x*sqrt(x))*(6 * sqrt(x) + 3 *sqrt(x));
}

double secantei(double l, double r) {
	double   x0=l, x1 = r,x2;
	if (l == 1) //din cauza ca pentru intervalul [1,1.25] aproximarea ma ducea intr-o solutie gresita, am ales ca punt de plecare, un punct mai apropiat de solutie (solutia am vazut-o cu metoda 1)
		x1 = 1.05;
	if (l == 2.5)//echivalent cu intervalul [2.5,3]
		x1 = 2.66;
	int it;


	for (it = 0; it < maxiters; it++) {
		x2 = (x0*f(x1)-x1*f(x0)) / (f(x1) - f(x0));
	    if (abs(x2-x1) <= epsilon && abs(f(x1))< epsilon)
		{
			return x2;
		}
		x0 = x1;
		x1 = x2;
	}
	//cout << "Warning: Not able to find solution with precision of " << sigma << " within " << maxiters << " iterations\n";
	return x2;
}

double falsei(double s, double t)
{
	double r, fr;
	int n, side = 0;
	/* starting values at endpoints of interval */
	double fs = f(s);
	double ft = f(t);
	for (n = 0; n < maxiters; n++)
	{

		r = (fs*t - ft*s) / (fs - ft);
		if (abs(t - s) < sigma*abs(t + s)) break;
		fr = f(r);

		if (fr * ft > 0)
		{
			/* fr and ft have same sign, copy r to t */
			t = r; ft = fr;
			if (side == -1) fs /= 2;
			side = -1;
		}
		else if (fs * fr > 0)
		{
			/* fr and fs have same sign, copy r to s */
			s = r;  fs = fr;
			if (side == +1) ft /= 2;
			side = +1;
		}
		else
		{
			/* fr * f_ very small (looks like zero) */
			break;
		}
	}
	return r;
}

double bisectiei(double l, double r) {
	double c;
	for (int i = 0; i < maxiters; ++i) {
		c = (l + r) / 2;
		if (f(c) == 0 || (r - l) / 2 < sigma) {
			return c;
		}
		if (f(c)*f(l) >= 0)
			l = c;
		else
			r = c;
	}
	cout << "Warning: Not able to find solution with precision of " << sigma << " within " << maxiters << " iterations\n";
	return c;
}

double coardei(double l, double r) {
	double x0 = l, x1 = r, x = x0, y = f(x);
	int i = 0;
	while ((i <= maxiters) && ((y<-epsilon) || (y>epsilon)))
	{
		x = x0 - f(x0)*(x1 - x0) / (f(x1) - f(x0));
		y = f(x);
		if (f(x0)*y<0)
			x1 = x;
		else
			x0 = x;
		i++;
	}
	if (i>maxiters) {
		cout << "Warning: Not able to find solution with precision of " << sigma << " within " << maxiters << " iterations\n";
	}
	return x;
}

double newton(double l, double r) {
	if (f(l) == 0) return l;
	if (f(r) == 0) return r;
	double x0 = (r+l)/2.0;
	if (l == 2.75) //din cauza ca pentru intervalul [2.75,3] aproximarea ma ducea intr-o solutie gresita, am ales ca punt de plecare, un punct mai apropiat de solutie (solutia am vazut-o cu metoda 1)
		x0 = 2.75;
		
	int nmax = 1000;
	for (int i = 0; i < nmax; ++i) {
		double y = f(x0);
		double y1 = f1(x0);
		if (abs(y1) < epsilon) {
			cout << "denominator too small";
		}
		double x1 = x0 - y / y1;
		if (abs(x1 - x0)  < sigma && y< sigma && (x1 >= l && x1 <= r)) {
			return x1;
		}
		x0 = x1;
	}
	//cout << "Warning: Not able to find solution to within the desired tolerance of " << sigma;
	return x0;
}

inline string meniu() {
	return "Avand functia 4 * exp(-x)*sin(6 * x*sqrt(x)) - (1 / 5.0), putem folosi urmatoarele metode pentru a calcula aproximarile:\n\n"
		"1. Bisectiei\n"
		"2. Falsei pozitii\n"
		"3. Coardei\n"
		"4. Secantei\n"
		"5. Newton\n\n"
		"Metoda preferata:\n";
}

int main()
{
	while(1)
	{
	    int met;
		cout << meniu();
		cin >> met;
		for (int i = 0; i < intervals; ++i) {
			double c = low + (high - low) / intervals*i;
			double d = low + (high - low) / intervals*(i + 1);
			if (f(c)*f(d) <= 0) {
				double c1 = c;
				double d1 = d;
				cout << "[" << c << ", " << d << "] ~ " << mets[met - 1](c, d) << endl;
			}
		}
	}
	return 0;
}


