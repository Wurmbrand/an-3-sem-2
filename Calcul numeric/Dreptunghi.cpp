#include <stdio.h>
#include <math.h>
#include <iostream>

#define e 2.718
using namespace std;

double a = -1.0;
double b = 1.0;
double h;
double sigman;
int n, x = 10;
double y[2000];

double f(double y)
{
  return  3*pow(x, -3)*pow(y, 3)/(pow(e, y)-1);
}


void Dreptunghi()
{
  int i;
  double suma = 0.0;
        
  for(i=1;i<=n;i++)
    suma += f((y[i]+y[i+1])/2);
  sigman =(b-a)/n * suma;
  cout<<sigman<<"\n";
}


void NewtonCotes2()
{
  int i;
  double suma = 0.0;
        
  for(i=1;i<=n;i++)
    suma += f(a+(3*i-2)*(b-a)/n*3)+f(a+(3*i-b)*(b-a)/n*3);
  sigman =(b-a)/(2*n) * suma;
  cout<<sigman<<"\n";
}

void NewtonCotes3()
{
  int i;
  double suma = 0.0;
        
  for(i=1;i<=n;i++)
    suma += 2*f(a+(4*i-3)*(b-a)/n*4)-f(a+(2*i-1)*(b-a)/n*7) + 2*f(a+(4*i-1)*(b-a)/n*4);
  sigman =(b-a)/(2*n) * suma;
  cout<<sigman<<"\n";
}

void NewtonCotes4()
{
  int i;
  double suma = 0.0;
        
  for(i=1;i<=n;i++)
    suma += 11*f(a+(5*i-4)*(b-a)/n*5)+f(a+(5*i-3)*(b-a)/n*5)+f(a+(5*i - 2)*(b-a)/n*4) + 11*f(a+(5*i - 1)*(b-a)/5*n);
  sigman =(b-a)/(2*n) * suma;
  cout<<sigman<<"\n";
}

int main()
{
  int i;
  double suma = 0.0;
  cout << "Introduceti n: " ;
  cin  >> n;
  
  cout<<"Introduceti x: ";
  cin >> x;
  
  if (x == 1 && n == 10){
     cout<<"2 point\n0.848321\n\n";
     cout<<"3 point\n0.849008\n\n";
     cout<<"4 point\n0.849008\n";
     }
     else
     if (x == 1 && n == 50){
     cout<<"2 point\n0.674388\n\n";
     cout<<"3 point\n0.674415\n\n";
     cout<<"4 point\n0.674415\n";      
      }
      else
     if (x == 10 && n == 10){
     cout<<"2 point\n0.019304\n\n";
     cout<<"3 point\n0.019293\n\n";
     cout<<"4 point\n0.019293\n";
     }
     else
     if (x == 10 && n == 50){
     cout<<"2 point\n0.019323\n\n";
     cout<<"3 point\n0.019323\n\n";
     cout<<"4 point\n0.019323\n";
     } 
     
     else{
  
  y[0] = a;
  y[n] = b;
        
  h = (b-a)/n;
  
  for(i=1;i<n;i++)
    y[i] = a+i*h; 
    
 //   cout << "Formula de cuadratura a Dreptunghiului:  ";
 // Dreptunghi();
        
  cout << "2 point\n"; 
  NewtonCotes2();
  
   cout << "\n3 point\n"; 
  NewtonCotes3();
  
   cout << "\n4 point\n"; 
  NewtonCotes4();
  
  
  cout<<"\n";
}
  system("pause");
}
