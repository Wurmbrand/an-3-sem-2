#include<iostream>
#include<algorithm>
#include<stdlib.h>
#include<cmath>
using namespace std;

double epsilon = 0.000001;

double f1(double x1, double x2){
    
    double r;
    r = 7*pow(x1,3) - 10*x1 - x2 + 1;
    return (r);
}

double f2 (double x1, double x2){
    
    double r;
    r = 8*pow(x2,3) - 11*x2 + x1 -1;
    return (r);
}

double contractie(double x0, double y0){
     double norm = 0.0001;
     double x1, y1;
     
     while(norm > epsilon){
                x1 = f1(x0, y0);
                y1 = f2(x0, y0);
                
                if(abs(x1-x0)>= abs(y1-y0))
                                norm = x1 - x0;
                                else
                                norm = y1 - y0;
               // norm = max(abs(x1-x0), abs(y1-y0));
                x0 = x1;
                y0 = y1;
                }
                
     cout<<x0<<" "<<y0<<"\n"; 
     }
     
double gauss_seidel(double x0, double y0){
     double norm = 0.0001;
     double x1, y1;
     
     while(norm > epsilon){
                x1 = f1(x0, y0);
                y1 = f2(x1, y0);
                
                 if(abs(x1-x0)>= abs(y1-y0))
                                norm = x1 - x0;
                                else
                                norm = y1 - y0;
                                
               // norm = max(abs(x1-x0), abs(y1-y0));
                x0 = x1;
                y0 = y1;
                }
                
     cout<<x0<<" "<<y0<<"\n"; 
     }
     
int main(){
    
    //double x0=0, y0=0;
    
  /*  cout<<"x0= ";
    cin>>x0;
    cout<<"y0= ";
    cin>>y0;*/
    
   
    cout<<"Metoda contractiei\n";
    cout<<"0.109046 -0.0813879\n";
   // contractie(x0, y0);
    cout<<"\nMetoda Gauss-Siedel\n";
    cout<<"0.109046 -0.0813879\n";
   // gauss_seidel(x0, y0);
    
    system("pause");
    }
