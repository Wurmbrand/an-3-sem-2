#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

int n, p;
double a[100][100];
double l[100][100];
double u[100][100];
double y[100];
double b[100];
double x[100];

int combinari(int n,int k){
     long s1=1, s2=1;
  if (k==0)
    return 1;
  else
    if (k>n)
      return 0;
    else{
      return (combinari(n-1,k)+combinari(n-1,k-1));
  /*  for(int i=n-k; i<=n; i++)
        s1=s1*i;

    for(int i=1; i<=k; i++)
        s2=s2*i;*/
}
 //   return s1/s2;
}

//Combinarile mele
void calculeazaMatrice(int n, int p) {
  int i, j;
  int comb_n, comb_k;
  for (i =1; i <=n; i++)
    for (j=1; j<= n; j++){
      comb_n = p + j - 1 ;
      comb_k = i - 1;
      a[i][j] = combinari (comb_n,comb_k);
    }

  // Afisare matrice
  cout << "Matricea A:" << endl;
  for (i =1; i <=n; i++) {
    for (j=1; j<= n; j++)
      cout << a[i][j] << " ";
    cout << endl;
  }
  cout << endl ;
}

void calculeazaLU() {
  int k,i;
  double suma;

  // Primul pas
  for(int i=1; i<=n; i++)
    l[i][1] = a[i][1];

  u[1][1] = 1;

  for(int j=2; j<=n; j++) {
    if (l[1][1] != 0)
      u[1][j] = a[1][j] / l[1][1];
    else {
      cout << "Metoda nu se aplica ";
      return;
    }
  }

  for(k = 2; k <= n; k++) {
//        l[k][k] = 1;

    for(i = k; i <= n; i++) {
      //L
      suma = 0;

      for(p=1; p<=k-1; p++)
        suma += l[i][p] * u[p][k];
      l[i][k] = a[i][k] - suma;
    }


    u[k][k] = 1;

    // U
    for (int j= k+1; j<=n; j++) {
      suma = 0;

      for (int p=1; p<=k-1; p++)
        suma += l[k][p] * u[p][j];


      int numarator = a[k][j] - suma;
      if (l[k][k] != 0)
        u[k][j] = numarator / l[k][k];
      else {
        cout << "Metoda nu se aplica";
        return;
      }
    }
  }

  // Afisare L
  cout << "Matricea L:" << endl ;
  for (int i = 1; i <=n; i++) {
    for (int j=1; j <= n; j++)
      cout << l[i][j] << " ";
    cout << endl;
  }
  cout << endl;

  // Afisare U
  cout << "Matricea U:"  << endl ;
  for (int i = 1; i <=n; i++) {
    for (int j=1; j <= n; j++)
      cout << u[i][j] << " ";
    cout << endl;
  }
  cout << endl;
}

//sistem
void calculeazaSistem() {
  double suma, numarator;
  int i, k;
  // Initializare vectori coloana
  for (int i = 1; i <= n; i++) {
    b[i] = 1;
    y[i] = 0;
  }

  //calcul y
  for(i = 1; i<=n; i++) {
    suma = 0;
    for(k = 1; k <= i-1; k++)
      suma += l[i][k] * y[k];
      
    numarator = b[i] - suma;
    
    if ( l[i][i] != 0)
      y[i] = numarator / l[i][i];
    else {
      cout << "Metoda nu se aplica!";
      return;
    }
  }
}

double sol[100];


//Calcul x
void calculeazaX() {
  int i, j, k;
  double suma;
  x[1] = 0;

  for (i = n; i >= 1; i--) {
    suma = 0;
    for (int k = i+1; k<=n; k++)
      suma += u[i][k] * x[k];
    x[i] = y[i] - suma;
  }

  // Afisare X
  cout << "Solutia este: ";
  for (int i =1; i <= n; i++) {
  //  sol[i] = x[i];
    cout << x[i] << " ";
  }
  cout << endl;
}

int main() {
  cout << "Introduceti n: " ;
  cin  >> n;
  cout << endl << "Introduceti p: ";
  cin >>p ;
  calculeazaMatrice(n,p);
  calculeazaLU();
  calculeazaSistem();
  calculeazaX();

  system("pause");
}
