#include <iostream>
#include <cmath>

using namespace std;
int n;
double fa, fb, fm, product1, product2;

double function (double x)
{
	double r;
	r = (4*pow(2.718,-x)*sin(6*x*sqrt(x)))-0.2;
	return (r);
}

double bisection(double a, double b){
      double m;
            
        if(b - a >= 0.001){
          while(b - a >= 0.1){
                
                 m=(a+b)/2;
                           
                          if(function(a)*function(m) < 0)
                                b = m;
                                  
                          if(function(m)*function(b) < 0)
                                a = m;
                                      
          
          }
        return m;
        }
        else
       system("pause");
        
}
          
          
int main () {
    
    int i;
    
    double upper[10]={0.1, 0.7, 1.1, 1.4, 1.7, 1.9, 2.2, 2.5, 2.7, 3};
    double lower[10]={0, 0.5, 0.9, 1.2, 1.5, 1.75, 2, 2.25, 2.5, 2.7};
    
    for(i=0; i<10; i++){
    if(function(lower[i]) * function(upper[i]) < 0){
    cout<<bisection(lower[i], upper[i])<<endl;
    
}
    }
    system("pause");
}
