#include <iostream>
#include <cmath>
#include<math.h>

using namespace std;


double f (double x)
{
	double r;
	
	r= (4*pow(2.718,-x)*sin(6*x*sqrt(x)))-0.2;
	return (r);
}

   double chord(double a, double b){
 
    double x1, x2, c;

    c = a;
    x1 = b;
    x2 = (c*f(x1) - x1*f(c))/(f(x1)-f(c));
    
    while (abs(x1 - x2) >= 0.000001)  {
      x1 = x2;
      x2 = (c*f(x1) - x1*f(c))/(f(x1)-f(c));
    }
    return x2;
    
}
      
          
int main () {
    
    int i;
    
    double upper[13]={0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3};
    double lower[13]={0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75};
    
    
    for(i=0; i<12; i++){
 
    if(f(lower[i]) * f(upper[i]) < 0){
                    cout<<chord(lower[i], upper[i])<<endl;
    
}
    }
    system("pause");
}
