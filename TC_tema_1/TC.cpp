#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <utility>
#include <vector>

std::vector<std::vector<std::vector<std::pair<char,std::string> > > > tranz;
int iterator, no_states, start_state, no_final, no_alphabet, no_tranz;
char reader[255];
char alphabet[50], input_string[255];
char ch;
int final_states[50];

void read_from_file()
{
    //Open the input file
    FILE *fp;
	fp = fopen("~NFA.txt", "r");
        /*
         * From MY writing convention the first row
         * is an integer number (no_states) of the
         * states of our ~NFA and the states are called
         * from 0 to (no_states - 1)
         */
        fscanf(fp, "%s", reader);
        /*
         * The conversion into an integer value
         * because we read form file a string
         */
        no_states = atoi(reader);
        //tranz.resize(no_states);
        for(int i = 0 ; i < no_states ; i++)
        {
        	std::vector<std::vector<std::pair<char,std::string> > > temp;
        	//temp.resize(no_states);
        	for(int j = 0 ; j < no_states ; j++)
        	{
        		std::vector<std::pair<char,std::string> > temp2;
        		//temp2.resize(no_alphabet);
        		temp.push_back(temp2);
        	}
        	tranz.push_back(temp);
        }

	/*
	 * On the second row we assume that the start state is
	 */
	fscanf(fp, "%s", reader);
	start_state = atoi(reader);
	/*
	 * On the third row is written firstly the number
	 * of the final states, then ":" and the final states ID
	 */
	fscanf(fp, "%s", reader);
	no_final = atoi(reader);
	fscanf(fp, "%s", reader);//here we pass :

	//Here I take the final states
	for(iterator=0; iterator<no_final; iterator++){
		fscanf(fp, "%s", reader);
		final_states[iterator] = atoi(reader);
	}

	fscanf(fp, "%s", reader);
	no_alphabet = atoi(reader);
	fscanf(fp, "%s", reader);//here we pass :

	//Here I take the alphabet
	for(iterator=0; iterator<no_alphabet; iterator++){
		alphabet[iterator] = getc(fp);
		if (alphabet[iterator] == ' ')
			iterator--;
	}

	fscanf(fp, "%s", reader);
        /*
		 * No_Tranz
         */
    no_tranz = atoi(reader);

    for(int i=0; i<no_tranz; i++)
    {
    	int s1=0, s2=0;
    	char c;
    	fscanf(fp, "%s", reader);
    	s1 = atoi(reader);

    	fscanf(fp, "%s", reader);
    	c = reader[0];

    	fscanf(fp, "%s", reader);
    	s2 = atoi(reader);

    	fscanf(fp, "%s", reader);
    	std::string s(reader);

    	//printf("%d %d %s\n", s1, s2, s.c_str());

    	std::pair<char,std::string> p1;
    	p1.first = c;
    	p1.second = s.c_str();
    	tranz.at(s1).at(s2).push_back(p1);
    }

    for(int i = 0 ; i < no_states ; i ++)
    {
    	std::vector<std::vector<std::pair<char,std::string> > > vec1 = tranz.at(i);
    	for(int j = 0 ; j < no_states ; j++)
    	{
    		std::vector<std::pair<char,std::string> > vec2 = vec1.at(j);
    		for(int k = 0 ; k < vec2.size() ; k++)
    		{

    			std::pair<char,std::string> p = vec2.at(k);
    			char c = p.first;
    			std::string s = p.second;
    			printf("%c - %s\n", c, s.c_str());
    		}
    	}
    }

    fscanf(fp, "%s", input_string);
    printf("\nThe input is : %s",input_string);


//----------------------------------DEBUG-------------------------------------
	printf("\nThe number of the states: %d", no_states);
	printf("\nThe start state of the NFA is :%d", start_state);

	printf("\nThe final states are: ");
	for(iterator=0; iterator<no_final-1; iterator++)
		printf("%d, ", final_states[iterator]);
	printf("%d", final_states[no_final-1]);

	printf("\nThe alphabet is: ");
	for(iterator=0; iterator<no_alphabet-1; iterator++)
        printf("%c, ", alphabet[iterator]);
    printf("%c", alphabet[no_alphabet-1]);

 	printf("\nNo_tranz: %d\n", no_tranz);

 	int siz = tranz.at(0).at(2).size();
}

void process_input(char input[255], int current, std::string output, int lev)
{
    if(lev==100)
    {
        printf("Peste 100 de pasi recursivi\n");
        return;
    }
	//printf("%d %c\n", current, input[0]);

	for(int i = 0 ; i < no_final ; i++)
	{
		if(final_states[i] == current && input[0]=='\0')
		{
			printf("%s\n", output.c_str());
		}
	}

	if(input[0]=='\0')
		return;
	//printf("stari %d\n", no_states);

	for(int i = 0 ; i < no_states ; i++)
	{
		int siz = tranz.at(current).at(i).size();
		//printf("size tranz curent->i%d %d\n", siz, i);
		for(int j = 0 ; j < tranz.at(current).at(i).size() ; j++)
		{
			//printf("simbol %c\n", tranz.at(current).at(i).at(j).first);
			if(tranz.at(current).at(i).at(j).first == input[0])
			{
				output += " ";
				output += tranz.at(current).at(i).at(j).second;
				process_input(input+1,i,output, lev+1);
			}
			else if(tranz.at(current).at(i).at(j).first == '~')
			{
				output += " ";
				output += tranz.at(current).at(i).at(j).second;
				process_input(input,i,output, lev+1);
			}
			else continue;
		}
	}
}

int main()
{
	std::string out = "";
	read_from_file();
	process_input(input_string,start_state,out,0);
	printf("\n");

return 0;
}
