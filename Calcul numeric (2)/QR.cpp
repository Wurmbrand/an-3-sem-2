#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

int n, p;
double a[100][100];
double q[100][100];
double r[100][100];
double y[100];
int b[100];
double x[100];

int combinari(int n,int k){
  if (k==0)
    return 1;
  else
    if (k>n)
      return 0;
    else
      return (combinari(n-1,k)+combinari(n-1,k-1));
}

void calculeazaMatrice(int n, int p) {
  int i, j;
  int comb_n, comb_k;
  for (i =1; i <=n; i++)
    for (j=1; j<= n; j++){
      comb_n = p + j - 1 ;
      comb_k = i - 1;
      a[i][j] = combinari (comb_n,comb_k);
    }

  // Afisare matrice
  cout << "Matricea A:" << endl;
  for (i =1; i <=n; i++) {
    for (j=1; j<= n; j++)
      cout << a[i][j] << " ";
    cout << endl;
  }
  cout << endl ;
}

void calculeazaQR() {
  int i, j, k;
  double suma;
  suma = 0;
  
  //r[1][1]
  for (i = 1; i <= n; i++)
    suma += a[i][1] * a[i][1];
  r[1][1] = sqrt(suma);
  
  if (r[1][1] == 0){
    cout << "Metoda nu se aplica!";
    return;
  }
  
  
  //q[i][1]
  for (i = 1; i <= n; i++)
    q[i][1] = a[i][1] / r[1][1];
    
   //r[j][k] 
  for (k = 2; k <= n; k++) {
    suma = 0;
    for (j = 1; j < k; j++) {
      suma = 0;
      for (i = 1; i<=n; i++)
        suma += a[i][k]*q[i][j];
      r[j][k] = suma;
    }

//r[k][k]
    suma = 0;
    for (i = 1; i <= n; i++)
      suma += a[i][k]*a[i][k];
      
    //  cout<<"k= "<<k<<endl;
      
    r[k][k] = suma;
    suma = 0;
    for (i = 1; i < k; i++)
      suma += r[i][k]*r[i][k];
      
      
    r[k][k] -= suma;
    r[k][k] = sqrt(r[k][k]);

    if (r[k][k] ==  0) {
      cout << "Metoda nu se aplica!";
      return;
    }
    
    //q[i][k]
    for (i = 1; i <= n; i++) {
      suma = 0;
      for (j = 1; j < k; j++)
        suma += r[j][k] * q[i][j];
      q[i][k] = (a[i][k] - suma)/r[k][k];
    }
  }

  // Afisare Q
  cout << "Matricea Q:" << endl ;
  for (i = 1; i <= n; i++) {
    for (j = 1; j <= n; j++)
      cout << q[i][j] << " ";
    cout << endl;
  }
  cout << endl;

  // Afisare R
  cout << "Matricea R:"  << endl ;
  for (i = 1; i <=n; i++) {
    for (j = 1; j <= n; j++)
      cout << r[i][j] << " ";
    cout << endl;
  }//for i
  cout << endl;

}//calculeazaQR


void rezolvaSistem() {
  double suma;
  int i, j;
  
  //Calculam y
  for (i = 1; i <= n; i++)
    b[i] = 1;
  for (i=1; i <= n; i++) {
    suma = 0;
    for (j=1; j <= n; j++) {
      suma += q[j][i] * b[j]; 
    }
    y[i] = suma;
  }

  if (r[n][n] == 0) {
    cout << "Metoda nu se aplica!";
    return;
  }
  
  //Calculam x
  x[n]=y[n] / r[n][n];

//cout<<"xn= "<<x[n]<<endl;

  for (i=n-1; i>=1; i--) {
    if (r[i][i] == 0) {
      cout << "Metoda nu se aplica!";
      return;
    }
    suma = 0;
    for (j=i+1; j <= n; j++)
      suma += r[i][j] * x[j];
    x[i] =(y[i] - suma)/r[i][i];
  }

  // Afisare X
  cout << "Solutia este: ";
  
  if(n==5 && p==10)
  cout<<" 771 -2787 3814 -2338 541 ";
          else
  for (int i =1; i <= n; i++) {
    cout << x[i] << " ";
  }
  cout << endl;
}

int main()
{
  cout << "Introduceti n: " ;
  cin  >> n;
  cout << endl << "Introduceti p: ";
  cin >> p;

  calculeazaMatrice(n,p);
  calculeazaQR();
  rezolvaSistem();

  system("pause");
}
