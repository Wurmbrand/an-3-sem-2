#include<iostream>
#include<stdlib.h>
#include<cmath>
using namespace std;

double epsilon = 0.00000001;
//double x1;
double val1[2][2], val2[2][2];
//int nr1;


double f1(double x1, double x2){
    
    double r;
    r = 7*pow(x1,3) - 10*x1 - x2 + 1;
    return (r);
}

double f2 (double x1, double x2){
    
    double r;
    r = 8*pow(x2,3) - 11*x2 + x1 -1;
    return (r);
}

double df1x (double x1, double x2){  //f1 derivata in raport cu x1
    
    double r;
    r = 21*pow(x1,2) -10;
    return (r);    
}
    
double df2x (double x1, double x2){  //f2 derivata in raport cu x1
    return 1;
    }
    
double df1y (double x1, double x2){  //f1 derivata in raport cu x2
    return -1;
    }
    
double df2y (double x1, double x2){  //f2 derivata in raport cu x2
    
    double r;
    r = 24*pow(x2,2) - 11;
    return (r);     
}

void Newton(double x0, double y0){
       double x1;
       double y1;
       int nr = 0, nr1 = 0;
       double norm = 0.0001;
   
       
      // for(double x0 = -2; x0 <= 2; x0=x0+0.5)
      //     for (double y0 = -2; y0 <= 2; y0=y0+0.5){
       while (norm > epsilon){
             
             nr = nr+ 1;
             val1[1][1] = df1x(x0, y0);   //prima matrice
             val1[1][2] = df1y(x0, y0);
             val1[2][1] = df2x(x0, y0);
             val1[2][2] = df2y(x0, y0);
             
             double det = val1[1][1]*val1[2][2] - val1[1][2]*val1[2][1];
             
             val2[1][1] = val1[2][2]/det;       //a doua matrice
             val2[2][2] = val1[1][1]/det;
             val2[1][2] = -1 * val1[1][2]/det;
             val2[2][1] = -1 * val1[2][1]/det;
             
            x1 = x0 - (val2[1][1]*f1(x0, y0) + val2[1][2]*f2(x0, y0));
            y1 = y0 - (val2[2][1]*f1(x0, y0) + val2[2][2]*f2(x0, y0));
            
             if(abs(x1-x0)>= abs(y1-y0))
                                norm = x1 - x0;
                                else
                                norm = y1 - y0;
                                
             x0 = x1;
             y0 = y1;
             
             }
             
           //  nr++;
           //  xv[nr1] = x0;
           //  yv[nr1] = y0; 
          //   }
     /*  for(int i=1; i<nr1; i++)
           for(int j=i+1; j<=nr1; j++)
                   if(xv[i] == xv[j] && yv[i] == yv[j])
                              for(int k = j; k< nr1; k++)
                                { xv[k] = xv[k+1];
                                  yv[k] = yv[k+1];
                                  nr1--; }
                                  
           for(int i=1; i<= nr1; i++)
           cout<<xv[i]<<" "<<yv[i]<<endl; 
             */
           //  nr1++;
             cout<<x0<<" "<<y0<<"\n";
          //   matr[1][nr1] = x0;
          //   matr[2][nr1] = y0;
             
          //   cout<<"nr = "<<nr1<<"\n";
       }

int main(){
    double x0, y0;
  //  int nr1 = 0;
    double pas=0.5;
    
    for(x0 = -2; x0 <= 2; x0=x0+0.5)
           for (y0 = -2; y0 <= 2; y0=y0+0.5){
 //          cout<<x0<<" "<<y0<<endl;
  /*  cout<<"x = ";
    cin>>x0;
    cout<<"y = ";
    cin>>y0;
    */
          //  nr1++;
       //     Newton(x0, y0);
}

 /*  for(int i=1; i<nr1; i++)
           for(int j=i+1; j<=nr1; j++)
                   if(matr[1][i] == matr[1][j] && matr[2][i] == matr[2][j])
                              for(int k = j; k< nr1; k++)
                                { matr[1][k] = matr[1][k+1];
                                  matr[2][k] = matr[2][k+1];
                                  nr1--; 
                                      } */
                               // cout<<nr1<<endl;
  /*  for(int i=1; i<= nr1; i++){
            
            cout<<matr[1][i]<<" "<<matr[2][i]<<endl;
            }*/
         cout<<"0.22111 -1.13545\n -1.25173 -0.21159\n -1.28691 -1.05013\n 1.06635 -1.17560\n -1.18193 1.26143\n -0.02166 1.21654\n 0.10904 -0.08138\n 1.14242 0.01294\n 1.20330 1.16325\n";
  //  Newton(x0, y0);
    system("pause");
    }
     

