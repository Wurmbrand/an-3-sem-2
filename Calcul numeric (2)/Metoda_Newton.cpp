#include <iostream>
#include <cmath>
#include<math.h>

using namespace std;


double f (double x)
{
	double r;
	
	r= (4*pow(2.718,-x)*sin(6*x*sqrt(x)))-0.2;
	return (r);
}

double fderivat(double x)
{
    return 4*exp(-x)*(9*sqrt(x)*cos(6*x*sqrt(x))-sin(6*x*sqrt(x)));
}

double newton(double a, double b)
{
    double x;
    double c=(a + b)/2;         //mijlocul
    
    
     if(fderivat(c)==0){
            system("pause");
        }
        
    x=c-(f(c)/fderivat(c));
    
    while(abs(x-c)>=0.00000001){
                          
        c=x;
            
        x=c-(f(c)/fderivat(c));
    }
    return x;
}


int main () {
    
    int i;
    
    
    double upper[10]={0.1, 0.7, 1.1, 1.4, 1.7, 1.9, 2.2, 2.5, 2.7, 3};
    double lower[10]={0, 0.5, 0.9, 1.2, 1.5, 1.75, 2, 2.25, 2.5, 2.7};
    
    for(i=0; i<10; i++){
    if(f(lower[i]) * f(upper[i]) < 0){
    cout<<newton(lower[i], upper[i])<<endl;
    
}
    }
    system("pause");
}
