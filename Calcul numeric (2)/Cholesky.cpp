#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

int n, p;
int a[100][100];
double at[100][100];
double l[100][100];
double y[100];
double b[100];
int x[100];

int combinari(int n,int k){
  if (k==0)
    return 1;
  else
    if (k>n)
      return 0;
    else
      return (combinari(n-1,k)+combinari(n-1,k-1));
}
//Verificare simetrie
int matriceSimetrica() {
  int i, j;
  for (i = 1; i <= n; i++)
    for (j = i; j <= n; j++)
      if (a[i][j] != a[j][i])
        return 0;
  return 1;
}


int matricePozitivDefinita() {
  int j, k;
  double suma;
  for (j = 1; j <= n; j++) {
    suma = 0;
    for (k = 1; k < j; k++)
      suma += l[j][k] * l[j][k];
    if ((a[j][j] - suma) <= 0)
      return 0;
  }
  return 1;
}

void calculeazaMatrice(int n, int p) {
  int i, j;
  int comb_n, comb_k;
  for (i =1; i <=n; i++)
    for (j=1; j<= n; j++){
      comb_n = p + j - 1 ;
      comb_k = i - 1;
      a[i][j] = combinari(comb_n,comb_k);
    }

  // Matricea B
  for (i = 1; i <= n; i++)
    b[i] = 1;

  // Afisare matrice
  cout << "Matricea A:" << endl;
  for (i =1; i <=n; i++) {
    for (j=1; j<= n; j++)
      cout << a[i][j] << " ";
    cout << endl;
  }
  cout << endl ;
}


void calculeazaTranspusa() {
  int i, j;

  for (i = 1; i <= n; i++)
    for (j = 1; j <= n; j++)
      at[i][j] = a[j][i];
}



int AtA[100][100];  //at*A
double AtB[100];       //at*b
void recalculeazaMatrice() {
  int i, j, k;

  calculeazaTranspusa();

//At * A
  for (i = 1; i <= n; i++)
    for (j = 1; j <= n; j++) {
    //  AtA[i][j] = 0;
      for (k = 1; k <= n; k++)
        AtA[i][j] += at[i][k] * a[k][j];
    }

//At * b
  for (i = 1; i <= n; i++) {
   // AtA[i][j] = 0;
    for (k = 1; k <= n; k++)
      AtB[i] += at[i][k]*b[k];
  }

  //Inlocuieste A cu AtA si B cu AtB

  for (i = 1; i <= n; i++) {
    b[i] = AtB[i];
    for (j = 1; j <= n; j++)
      a[i][j] = AtA[i][j];
  }

  cout << "Matricea recalculata AtA:" << endl;
  for (i =1; i <=n; i++) {
    for (j=1; j<= n; j++)
      cout << a[i][j] << " ";
    cout << endl;
  }
  cout << endl ;

  cout << "Matricea recalculata AtB:" << endl;
  for (i =1; i <=n; i++)
    cout << b[i] << " ";
  cout << endl << endl ;
}



void calculeazaL() {
  int i, j, k;
  double suma, partial;

  for (j = 1; j <= n; j++) {
    suma = 0;

    for (k = 1; k < j; k++)
      suma += l[j][k]*l[j][k];

    l[j][j] = sqrt(a[j][j] - suma);

    if (l[j][j] == 0) {
      cout << "Metoda nu se aplica!";
      return;
    }

    for (i = j+1; i <= n; i++) {
      suma = 0;
      for (k = 1; k < j; k++)
        suma += l[i][k] * l[j][k];
      l[i][j] = (a[i][j] - suma) / l[j][j];
    }
  }

  // Afisare L
  cout << "Matricea L:" << endl ;
  for (i = 1; i <= n; i++) {
    for (j = 1; j <= n; j++)
      cout << l[i][j] << " ";
    cout << endl;
  }
  cout << endl;
}

void rezolvaSistem() {
  double suma;
  int i, j, k;

  for (i = 1; i <= n; i++) {
    //if (l[i][i] == 0){
    //  cout << "Metoda nu se aplica!";
    //  return;
    //}

    suma = 0;
    for (k = 1; k < i; k++)
      suma += l[i][k]*y[k];

      if(l[i][i]!=0)
                    y[i] = (b[i] - suma) / l[i][i];
  }

 /* for (i = n; i >= 1; i--) {
    if (l[i][i] == 0) {
      cout << "Metoda nu se aplica!";
      return;
    }
    */

    for (i = n; i >= 1; i--){
    suma = 0;
    for (k = i+1; k <= n; k ++){
        //cout<<x[5]<<"\n";
      suma += l[k][i]*x[k];
      }
    x[i] = (y[i] - suma) / l[i][i];
}
 // }

  // Afisare X
  cout << "Solutia este: ";

  for (int i =1; i <= n; i++) {
    cout << x[i] << " ";
  }
  cout << endl;
}


int main()
{
  cout << "Introduceti n: " ;
  cin  >> n;
  cout << endl << "Introduceti p: ";
  cin >> p;

  calculeazaMatrice(n,p);


  if (matriceSimetrica())
    calculeazaL();


  if ((!matriceSimetrica()) || (!matricePozitivDefinita())) {
    recalculeazaMatrice();
    calculeazaL();
  }


  rezolvaSistem();

  cout << endl ;
return 0;
 // system("pause");
}
