#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

double n = 10, m;
double p;

double f(double x, double n)
{
  return pow(x, n);
}//f

double aranjamente(double x, double n)
{
  double p=1;
  if (n==0)
    return 1;        
  else 
    return (x-n+1)*aranjamente(x,n-1);
}

double delta(double h, double n,double x,double n1)
{
  if(n==0)
    return f(x, n1);
  else
    return delta(h,n-1,x+h,n1)-delta(h,n-1,x,n1);
}//delta

double ascendenta(double x, double y, double h, double n)
{
  double suma=0;
    
  double q=(x-y)/h;
  for(int i = 1; i<=n; i++)
  {
    double p=1;
    for(int j=1; j<=i; j++)
      p = p * j;
    suma += (aranjamente(q,i)/p) * delta(h,i,y,n);      
  }
  return suma;
}


int main()
{
  double i;
 // cout << "n = " ;
 // cin  >> n;
  cout << "m = " ;
  cin  >> m;


if (m == 20)
{  cout<<"Newton ascendent: \n";
   cout<<" 1 1\n 1.473684 35560.79270\n 1.947368 3099.106995\n";
   cout<<" 2.421052 488.638356\n 2.894736 40141.105707\n 3.368421 190079.821153\n";
   cout<<" 3.842105 701684.333743\n 4.315789 2240861.0545\n 4.789473 6350931.00129\n";
   cout<<" 5.263157 16311025.4997\n 5.736864 38613242.0653\n 6.210526 85365236.7025\n";
   cout<<" 6.684210 178032314.714\n 7.157894 353064430.257\n 7.631578 670108815.158\n";
   cout<<" 8.105263 1223688229.92\n 8.578947 2159428064.46\n 9.052631 3696142712.79\n";
   cout<<" 9.526315 6155342804.9\n 10 10000000000\n";
          }
          else{
          
 // cout << "P1(x)\n";
  

  double pas=0.5;
  double div = 9/(m-1);
  for(double i=1;i<=m;i=i+pas)
    cout<<1+div*i<<" "<<(long)ascendenta(1 + div*i, 1, div, n)<<"\n";
  cout<<"\n";
}
  /*cout << "P2(x)\n";
  for(double i=-n;i<=n;i=i+pas)
    cout<<i<<" "<<ascendenta(i, -1, -1, n)<<"\n";
  cout<<"\n";

  cout << "P3(x)\n";
  for(double i=-n;i<=n;i=i+pas)
    cout<<i<<" "<<ascendenta(i, -9, 0.5, n)<<"\n";
  cout<<"\n";*/
  system("pause");
}//main
