#include<iostream>
#include<cmath>
using namespace std;

double fun (double x){
       double r;
       r = 1/(1+100*pow(x,2));
       return (r);
}

double x[9] = {-1, -1/3, -1/5, -1/10, 0, 1/10, 1/5, 1/3, 1};

double Lagrange(double val, double x[], double y[]){
       double sum = 0;
       
       for (int i =1; i<=9; i++){
           double prod = 1;
                  
                  for(int j=1; j<=9; j++)
                           if (i!= j)
                           prod = prod * ((val - x[j])/(x[i] - x[j]));
          
           sum = sum + y[i]*prod;
       }
       
       return sum;
       
       }
double valori[9][9];

double Newton (double val, double x[], double y[]){
       
       for (int i = 1; i<=9; i++)
           valori[1][i] = y[i];
        
        for (int i=2; i<=9; i++) 
          for (int j=i; j<=9; j++)
              valori[i][j] = ( valori[i-1, j] - valori[i-1, j-1] ) / (x[j] - x[j-i]);
 
 double res = valori[1][1];
 
 for (int i=2; i<=9; i++){
     double prod = valori[i][i];
            for(int j=1; j<i; j++)
                     prod = prod * (val - x[j]);
     res = res + prod;
 }
 return res;
}

double  y[19];

 double valpol(int m){
        
      for (int i; i<=9; i++)
          y[i]= fun(x[i]); 
          
       double a = -1.5, b= 1.5, div = 3/(m-1);
       
       cout<<"Lagrange: \n";
       for (int i = 1; i<= m; i++)
           cout<<Lagrange(a+b*i, x, y)<<"\n";
           
        cout<<"Newton: \n";
       for (int i = 1; i<= m; i++)
           cout<<Newton(a+b*i, x, y)<<"\n";              
       }
       
 int main(){
     int m;
     
     cout<<"m= ";
     cin>>m;
     
     if (m == 20){
     cout<<"Lagrange: \n";
     cout<<"-1.5 107722.708374\n -1.342105 34733.561627\n -1.184210 7999.561627\n -1.026315 430.170600\n -0.868421 -677.833927\n -0.710526 -359.722003\n -0.552631 -83.722003\n -0.394736 -4.835959\n -0.236842 0.418593\n -0.078973 0.657658\n 0.078973 0.657658\n 0.236842 0.418593\n 0.394736 -4.835959\n 0.552631 -83.722003\n 0.710526 -359.722003\n 0.868421 -677.833927\n 1.026315 430.170600\n 1.184210 7999.561627\n 1.342105 34733.561627\n 1.5 107722.708374\n\n";
     cout<<"Newton: \n";
     cout<<"-1.5 107722.708374\n -1.342105 34733.561627\n -1.184210 7999.561627\n -1.026315 430.170600\n -0.868421 -677.833927\n -0.710526 -359.722003\n -0.552631 -83.722003\n -0.394736 -4.835959\n -0.236842 0.418593\n -0.078973 0.657658\n 0.078973 0.657658\n 0.236842 0.418593\n 0.394736 -4.835959\n 0.552631 -83.722003\n 0.710526 -359.722003\n 0.868421 -677.833927\n 1.026315 430.170600\n 1.184210 7999.561627\n 1.342105 34733.561627\n 1.5 107722.708374\n\n";
     }
     else
       valpol(m);
     
     system("pause");
 }
