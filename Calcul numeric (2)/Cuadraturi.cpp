#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

double a = -1.0;
double b = 1.0;
double h;
double sigman;
int n;
double x[2000];

double f(double x)
{
  return pow(x,7) * sqrt(1-x*x) / pow(2-x, 6.5);
}

void trapez()
{
  int i;
  double suma = 0.0;
  for(i=0;i<n;i++)
    suma += f(x[i]) + f(x[i+1]);
    
  sigman = (b-a)/(2*n)*suma;
  cout<<sigman<<"\n";
}

void Simpson()
{
  int i;
  double suma = 0.0;
  for(i=0;i<n;i++)
    suma += (f(x[i]) + 4*f((x[i]+x[i+1])/2) + f(x[i+1]));
  sigman = (b-a)/(6*n) *suma;
  cout<<sigman<<"\n";
}
    
void Newton()
{
  int i;
  double suma = 0.0;
        
  for(i=0;i<n;i++)
    suma += (f(x[i+1]) +3*f((2*x[i+1]+x[i])/3) + 3*f((x[i+1]+2*x[i])/3) + f(x[i]));
  sigman = (b-a)/(8*n) * suma;
  cout<<sigman<<"\n";
}
    
void Boole()
{
  int i;
  double suma = 0.0;
        
  for(i=1;i<=n;i++)
    suma += 7*f(x[i-1])+32*f((3*x[i-1]+x[i])/4)+12*f((x[i-1]+x[i])/2)+32*f((x[i-1]+3*x[i])/4)+7*f(x[i]);
  sigman = (b-a)/(90*n) * suma;
  cout<<sigman<<"\n";
}

int main()
{
  int i;
  double suma = 0.0;
  cout << "Introduceti n: " ;
  cin  >> n;
  x[0] = a;
  x[n] = b;
        
  h = (b-a)/n;
  
  for(i=1;i<n;i++)
    x[i] = a+i*h; 
    
  cout <<"Formula de cuadratura a trapezului:  ";
  trapez();
        
  cout << "\n";
        
  cout <<"Formula de cuadratura Simpson:  ";
  Simpson();
        
  cout << "\n";
        
  cout << "Formula de cuadratura Newton:  ";
  Newton();
        
  cout << "\n";
        
  cout << "Formula de cuadratura Boole:  ";
  Boole();

  system("pause");
}
