#include <iostream>
#include <cmath>
#include<math.h>

using namespace std;


double f (double x)
{
	double r;
	
	r= (4*pow(2.718,-x)*sin(6*x*sqrt(x)))-0.2;
	return (r);
}

   double secanta(double a, double b){
  
    double x1, x2, x3;

    x1 = a;
    x2 = b;
    x3 = (x1*f(x2) - x2*f(x1))/(f(x2)-f(x1));
    
    while (abs(x1 - x2) >= 0.000001 && abs(f(x3))>=0.000001)  {
      x1 = x2;
      x2 = x3;
      x3 = (x1*f(x2) - x2*f(x1))/(f(x2)-f(x1));
    }
    return x3;
    
}
      
          
int main () {
    
    int i;
    
    double upper[10]={0.1, 0.7, 1.1, 1.4, 1.7, 1.9, 2.2, 2.5, 2.7, 3};
    double lower[10]={0, 0.5, 0.9, 1.2, 1.5, 1.75, 2, 2.25, 2.5, 2.7};
    
    for(i=0; i<10; i++){
    if(f(lower[i]) * f(upper[i]) < 0){
    cout<<secanta(lower[i], upper[i])<<endl;
    
}
    }
    system("pause");
}
