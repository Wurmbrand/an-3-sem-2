#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

double n, m;
double p;

double f(double x, double n)
{
  return pow(x, n);
}//f

double combinari(double n, double k)
{
  if (k==0)
    return 1;
  else
    if (k>n)
      return 0;
    else
      return (combinari(n-1,k)+combinari(n-1,k-1));
}//combinari

double aranjamente(double x, double n)
{
  double p=1;
  if (n==0)
    return 1;        
  else 
    return (x-n+1)*aranjamente(x,n-1);
}

double delta(double h, double n,double x,double n1)
{
  if(n==0)
    return f(x, n1);
  else
    return delta(h,n-1,x+h,n1)-delta(h,n-1,x,n1);
}//delta

double nablah( double h,double i, double x)
{
  if (i==0)
    return f(x,n);
  else
    return nablah(h,i-1,x)-nablah(h,i-1,x-h);
}

double ascendenta(double x, double y, double h, double n)
{
  double suma=0;
    
  double q=(x-y)/h;
  cout<<"q = "<<q<<"\n";
  for(int i = 1; i<=n; i++)
  {
    double p=1;
    for(int j=1; j<=i; j++)
      p = p * j;
    suma += (aranjamente(q,i)/p) * delta(h,i,y,n);      
  }
  return suma;
}

double descendenta(double x,double y,double h, double n)
{
  double suma=0;
    
  double q=(x-y)/h;
  for(int i=0;i<=n;i++)
  {
    double p=1;
    for(int j=1;j<=i;j++)
      p=p*j;
    if (i%2==1)
      suma=suma-(aranjamente(-q,i)/p)*nablah(h,i,y);
    else
      suma=suma+(aranjamente(-q,i)/p)*nablah(h,i,y);          
  }
  return suma;
}


int main()
{
  double i;
  cout << "n = " ;
  cin  >> n;
  cout << "m = " ;
  cin  >> m;

  cout << "P1(x)\n";
  double pas=0.5;
  for(double i=-n;i<=n;i=i+pas)
    cout<<i<<" "<<ascendenta(i, 1, 1, n)<<"\n";
  cout<<"\n";
  
  cout << "P2(x)\n";
  for(double i=-n;i<=n;i=i+pas)
    cout<<i<<" "<<ascendenta(i, -1, -1, n)<<"\n";
  cout<<"\n";

  cout << "P3(x)\n";
  for(double i=-n;i<=n;i=i+pas)
    cout<<i<<" "<<ascendenta(i, -9, 0.5, n)<<"\n";
  cout<<"\n";
  system("pause");
}//main
